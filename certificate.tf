
resource "aws_acm_certificate" "cert" {
  provider          = aws-acm
  domain_name       = var.domain
  validation_method = "DNS"
}

