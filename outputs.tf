
output "cert_arn" {
  value = aws_acm_certificate.cert.arn
}
output "cert_domain_name" {
  value = aws_acm_certificate.cert.domain_name
}

output "resource_record_types" {
  value = [for dov in aws_acm_certificate.cert.domain_validation_options: dov.resource_record_type]

}
output "resource_record_values" {
  value = [for dov in aws_acm_certificate.cert.domain_validation_options : dov.resource_record_value]
}
output "resource_record_names" {
  value = [for dov in aws_acm_certificate.cert.domain_validation_options: dov.resource_record_name]
}
