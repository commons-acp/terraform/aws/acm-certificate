

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws-acm = {
      source  = "hashicorp/aws"
      version = "> 3.70.0"
    }
    aws-main = {
      source  = "hashicorp/aws"
      version = "> 3.70.0"
    }
  }
}
